abstract class CommonPluginDAO {
  void runPlugin(
    context,
    var provider,
    var providerData,
    var dataModel,
    Function callBack,
  );

  Future<Map<String, dynamic>> getExtractedData(String pluginData);
}
