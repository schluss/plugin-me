import 'package:flutter/material.dart';
import 'package:me/constants/ui_constants.dart';
import 'package:me/model/translation_model.dart';

class BackwardButton extends StatefulWidget {
  final Locale? locale;
  final title;
  final Color color;
  final Alignment alignment;
  final int popCount;
  final Function? function;

  const BackwardButton({
    this.locale,
    this.title,
    this.color = UIConstants.gray50,
    this.alignment = Alignment.topLeft,
    this.popCount = 1,
    this.function,
  });

  @override
  _BackwardButtonState createState() => _BackwardButtonState();
}

class _BackwardButtonState extends State<BackwardButton> {
  int count = 0;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Align(
      alignment: widget.alignment,
      child: GestureDetector(
        onTap: () {
          (widget.function!() != null)
              ? widget.function!()
              : Navigator.of(context).popUntil(
                  (_) => count++ >= widget.popCount,
                );
        },
        child: Container(
          color: UIConstants.transparent,
          padding: EdgeInsets.only(
            left: height * 0.02,
            top: height * 0.02,
            bottom: height * 0.01,
          ),
          width: width * 0.4,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Icon(
                  Icons.keyboard_arrow_left,
                  color: widget.color,
                  size: width * 0.1,
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: height * 0.02),
                child: Text(
                  (widget.title == null || widget.title.isEmpty)
                      ? TranslationModel.getTranslation(
                          widget.locale,
                        )!
                          .backButtonText!
                      : widget.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: widget.color,
                    fontFamily: UIConstants.subFontFamily,
                    fontSize: width * 0.042,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
