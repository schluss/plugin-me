import 'package:flutter/material.dart';
import 'package:me/constants/ui_constants.dart';

import 'network_img.dart';

class ProviderLogo extends StatelessWidget {
  final String? imagePath;

  const ProviderLogo(
    this.imagePath,
  );

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Container(
      width: width * 0.17,
      height: width * 0.14,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              width: width * 0.13,
              height: width * 0.13,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: UIConstants.grayLight,
                  width: 1,
                ),
                color: UIConstants.gray100,
              ),
              alignment: Alignment.center,
              child: NetworkImg(
                imagePath,
                0.074,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
