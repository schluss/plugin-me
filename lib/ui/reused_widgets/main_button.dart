import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:me/constants/ui_constants.dart';

/// Types of the main button.
enum BtnType {
  active,
  disabled,
  awaited,
  outline,
  outlineDark,
  whitish,
  topFlatted,
  bottomFlatted,
}

class MainButton extends StatefulWidget {
  final bool isDisabled;
  final BtnType type;
  final String text;
  final Color? textColor;
  final Function? call;

  const MainButton({
    this.isDisabled = false,
    required this.type,
    required this.text,
    required this.call,
    this.textColor,
    Key? key,
  }) : super(key: key);

  @override
  State<MainButton> createState() => _MainButtonState();
}

class _MainButtonState extends State<MainButton> {
  bool _isDisabled = false;

  void _onPress() {
    if (_isDisabled || widget.type == BtnType.awaited || widget.type == BtnType.disabled) {
      return;
    }

    if (widget.isDisabled) {
      setState(() => _isDisabled = true);
    }

    // call the function
    if (widget.call != null) {
      widget.call!();
    }
  }

  @override
  Widget build(BuildContext context) {
    var type = _isDisabled ? BtnType.awaited : widget.type;
    var textColor = _getTextColor(type)!;

    double width = MediaQuery.of(context).size.width;
    double heightFactor = 0.15;
    var radius = const Radius.circular(8.0);

    return ButtonTheme(
      child: ConstrainedBox(
        constraints: BoxConstraints.tightFor(
          width: width,
          height: width * heightFactor,
        ),
        child: ElevatedButton(
          onPressed: _onPress,
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: type == BtnType.topFlatted
                  ? BorderRadius.only(bottomLeft: radius, bottomRight: radius)
                  : type == BtnType.bottomFlatted
                      ? BorderRadius.only(topLeft: radius, topRight: radius)
                      : BorderRadius.circular(8.0),
              side: BorderSide(
                color: type == BtnType.active
                    ? UIConstants.accentColor
                    : type == BtnType.whitish || type == BtnType.awaited
                        ? UIConstants.primaryColor
                        : type == BtnType.topFlatted || type == BtnType.bottomFlatted
                            ? UIConstants.transparentWhite
                            : UIConstants.paleLilac,
              ),
            ),
            backgroundColor: type == BtnType.active
                ? UIConstants.accentColor
                : type == BtnType.outline || type == BtnType.outlineDark
                    ? UIConstants.transparent
                    : type == BtnType.whitish
                        ? UIConstants.primaryColor
                        : type == BtnType.topFlatted || type == BtnType.bottomFlatted
                            ? UIConstants.transparentWhite
                            : UIConstants.paleLilac, // Colour.*/
            foregroundColor: widget.textColor ?? textColor, // Text/icons/overlay colour.
            elevation: type == BtnType.active ? 8.0 : 0.0, // Shadow size.
            shadowColor: type == BtnType.active ? UIConstants.watermelon : UIConstants.transparent,
            enableFeedback: true,
            tapTargetSize: MaterialTapTargetSize.padded,
            animationDuration: const Duration(milliseconds: 10),
          ),
          child: (type == BtnType.awaited || widget.isDisabled && _isDisabled)
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    _buildText(textColor),
                    Padding(
                      padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width * 0.025,
                      ),
                      child: SizedBox(
                        height: 8.0,
                        child: SpinKitThreeBounce(
                          color: textColor,
                          size: 8.0,
                        ),
                      ),
                    ),
                  ],
                )
              : _buildText(textColor),
        ),
      ),
    );
  }

  Text _buildText(Color textColor) {
    return Text(
      widget.text,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: MediaQuery.of(context).size.width * 0.045, fontWeight: FontWeight.w600, fontFamily: UIConstants.btnFontFamily, color: textColor),
    );
  }

  Color? _getTextColor(BtnType type) {
    Color? textColor;

    if (type == BtnType.active) {
      textColor = UIConstants.primaryColor;
    } else if (type == BtnType.awaited || type == BtnType.topFlatted || type == BtnType.bottomFlatted) {
      textColor = UIConstants.slateGray100;
    } else if (type == BtnType.disabled) {
      textColor = UIConstants.slateGray40;
    } else if (type == BtnType.outline || type == BtnType.whitish) {
      textColor = UIConstants.slateGray90;
    } else if (type == BtnType.outlineDark) {
      textColor = UIConstants.primaryColor;
    }
    return textColor;
  }
}
