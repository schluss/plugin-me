import 'package:flutter/material.dart';
import 'package:me/constants/ui_constants.dart';

class GroupHeader extends StatelessWidget {
  final String? title;
  final double leftPadding;

  const GroupHeader(
    this.title, {
    this.leftPadding = 0.075,
  });

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Container(
      width: width,
      padding: EdgeInsets.only(
        left: width * leftPadding,
        top: height * 0.0125,
        bottom: height * 0.0125,
        right: height * 0.008,
      ),
      decoration: BoxDecoration(
        color: UIConstants.paleGray,
        border: Border(
          top: BorderSide(color: UIConstants.paleGray),
          bottom: BorderSide(color: UIConstants.paleGray),
        ),
      ),
      child: Text(
        title!,
        maxLines: 1,
        style: TextStyle(
          fontFamily: UIConstants.subFontFamily,
          color: UIConstants.grayDarkest100,
          fontSize: width * 0.03,
          fontWeight: FontWeight.w600,
          fontStyle: FontStyle.normal,
          letterSpacing: 1,
        ),
      ),
    );
  }
}
