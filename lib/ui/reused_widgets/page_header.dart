import 'package:flutter/material.dart';
import 'package:me/constants/theme.dart';
import 'package:me/constants/ui_constants.dart';

import 'backward_button.dart';
import 'closing_button.dart';

class PageHeader extends StatelessWidget {
  final Locale locale;
  final String? pageTitle;
  final BackwardButton? backwardButton;
  final Color titleColor;
  final String? iconPath;
  final bool isDividerVisible;
  final Function? closeCall;
  final Function? backwardCall;

  const PageHeader(
    this.locale,
    this.pageTitle, {
    this.backwardButton,
    this.titleColor = UIConstants.black,
    this.iconPath,
    this.isDividerVisible = false,
    this.closeCall,
    this.backwardCall,
  });

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      decoration: BoxDecoration(
        color: UIConstants.primaryColor,
        borderRadius: mainBorderRadius,
      ),
      height: height * 0.1,
      alignment: Alignment.center,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxWidth: width * 0.55,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  iconPath != null
                      ? Image.asset(
                          iconPath!,
                          height: width * 0.045,
                        )
                      : Container(),
                  Flexible(
                    child: Text(
                      pageTitle!,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: titleColor,
                        fontSize: width * 0.04,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          backwardButton != null
              ? BackwardButton(
                  locale: locale,
                  function: backwardCall,
                )
              : Align(
                  alignment: Alignment.centerRight,
                  child: ClosingButton(
                    UIConstants.primaryColor,
                    UIConstants.accentColor,
                    () {
                      (closeCall != null) ? closeCall!() : Navigator.pop(context);
                    },
                  ),
                ),
          isDividerVisible
              ? Align(
                  alignment: Alignment.bottomCenter,
                  child: Divider(height: 1),
                )
              : Container(),
        ],
      ),
    );
  }
}
