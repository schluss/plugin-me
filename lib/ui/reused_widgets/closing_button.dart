import 'package:flutter/material.dart';
import 'package:me/constants/ui_constants.dart';

class ClosingButton extends StatelessWidget {
  final Color backgroundColor;
  final Color iconColor;
  final Function call;

  const ClosingButton(
    this.backgroundColor,
    this.iconColor,
    this.call,
  );

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        call();
      },
      child: Container(
        margin: EdgeInsets.only(
          right: MediaQuery.of(context).size.height * 0.02,
        ),
        color: backgroundColor,
        height: MediaQuery.of(context).size.height * 0.095,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.095,
          height: MediaQuery.of(context).size.width * 0.095,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: backgroundColor,
            border: Border.all(
              color: UIConstants.paleLilac,
              width: 0.5,
            ),
          ),
          child: Icon(
            Icons.close,
            color: iconColor,
            size: MediaQuery.of(context).size.width * 0.05,
          ),
        ),
      ),
    );
  }
}
