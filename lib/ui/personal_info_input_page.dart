import 'package:flutter/material.dart';
import 'package:me/constants/ui_constants.dart';
import 'package:me/model/translation_model.dart';
import 'package:me/ui/reused_widgets/group_header.dart';
import 'package:me/ui/reused_widgets/main_button.dart';
import 'package:me/ui/reused_widgets/page_header.dart';
import 'package:me/ui/reused_widgets/provider_logo.dart';
import 'package:me/ui/reused_widgets/user_info.dart';

class PersonalInfoInputPage extends StatefulWidget {
  final Locale locale;
  final provider;
  final providerData;
  final dataModel;
  final Function callBack;
  final String? pluginName;

  const PersonalInfoInputPage(
    this.locale,
    this.provider,
    this.providerData,
    this.dataModel,
    this.callBack, {
    this.pluginName,
    Key? key,
  }) : super(key: key);

  @override
  PersonalInfoInputPageState createState() => PersonalInfoInputPageState();
}

class PersonalInfoInputPageState extends State<PersonalInfoInputPage> {
  Map<String, List<TextEditingController>> textEditingControllers = {};

  String inputData = '';

  String? _title;
  bool isEdited = false;

  @override
  void initState() {
    _title = widget.provider['title'];

    //Todo : Need to Stop sending Datamap here. It will tight couple Plugin with Main app.
    //Initiate Map for store Text editing controllers
    widget.dataModel.dataMap.forEach((key, value) {
      var textControllersList = <TextEditingController>[];
      value.forEach((element) {
        var textEditingController = TextEditingController();
        textControllersList.add(textEditingController);
      });
      textEditingControllers.putIfAbsent(key, () => textControllersList);
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    // dispose textEditingControllers to prevent memory leaks
    textEditingControllers.forEach((key, value) {
      var textControllersList = value;
      for (var textEditingController in textControllersList) {
        textEditingController.dispose();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: UIConstants.primaryColor,
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              height: height,
              width: width,
              color: UIConstants.primaryColor,
              child: ListView(
                padding: EdgeInsets.only(
                  top: height * 0.1 + MediaQuery.of(context).padding.top,
                  bottom: height * 0.4,
                ),
                children: <Widget>[
                  SizedBox(height: height * 0.02),
                  Stack(
                    children: <Widget>[
                      UserInfo(
                        TranslationModel.getTranslation(widget.locale)!.providerInfoSrcTxt,
                        widget.providerData['name'],
                        heightScale: 0.064,
                        isTitleBold: true,
                        isSubTitleBold: true,
                        isEditable: false,
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: EdgeInsets.only(right: width * 0.05),
                          child: ProviderLogo(widget.providerData['logo']),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: height * 0.025),
                  ...buildChildren(
                    widget.dataModel,
                    width,
                    height,
                    context,
                  ),
                ],
              ),
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: height * 0.1,
                  color: UIConstants.paleLilac,
                ),
              ),
              PageHeader(
                widget.locale,
                _title,
              ),
            ],
          ),
        ],
      ),
      bottomNavigationBar: Container(
        color: UIConstants.primaryColor,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            /// Main button.
            Padding(
              padding: EdgeInsets.only(
                top: height * 0.03,
                bottom: height * 0.025,
                left: width * 0.05,
                right: width * 0.05,
              ),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: MainButton(
                  type: BtnType.active,
                  text: 'Opslaan',
                  call: () async {
                    _sendBackData();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Builds Children providers.
  List<Widget> buildChildren(
    var model,
    double width,
    double height,
    BuildContext context,
  ) {
    List<Widget> children;
    children = <Widget>[];

    /// Parses map without groups.
    if (model.dataMap.keys.length == 1 && model.dataMap.keys.first.isEmpty) {
      children.add(const GroupHeader(''));

      model.dataMap.values.forEach(
        (list) {
          list.forEach((element) {
            children.add(
              UserInfo(
                element.label.toUpperCase(),
                element.value ?? '',
              ),
            );
          });
        },
      );
    } else {
      /// Parses map with groups.
      model.dataMap.forEach(
        (key, value) {
          children.add(GroupHeader(key.toUpperCase()));
          final controllerMap = textEditingControllers[key]!.asMap();
          value.forEach((element) {
            int? index = value.indexOf(element);
            var textController = controllerMap[index!];
            children.add(
              UserInfo(
                element.label.toUpperCase(),
                element.value ?? '',
                call: textController,
              ),
            );
          });
        },
      );
    }
    return children;
  }

  /// Sends data back to the main app.
  void _sendBackData() {
    var values = StringBuffer();

    textEditingControllers.forEach((key, value) {
      for (var i = 0; i < value.length; i++) {
        values.write(value[i].text + ',');
      }
    });

    widget.callBack(widget.pluginName, values.toString(), context);
  }
}
