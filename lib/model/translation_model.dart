import 'package:flutter/cupertino.dart';
import 'package:me/util/translation_util.dart';

class TranslationModel {
  static final nl = 'NL';
  static final en = 'EN';
  static final uk = 'UK';

  final String? backButtonText;
  final String? providerInfoSrcTxt;
  final String? dataManualChangesBottomShtTitle;
  final String? dataManualChangesBottomShtTxt;
  final String? btnNoGoBackTxt;
  final String? dataManualChangesBottomShtBtnTxt;

  const TranslationModel({
    this.backButtonText,
    this.providerInfoSrcTxt,
    this.dataManualChangesBottomShtTitle,
    this.dataManualChangesBottomShtTxt,
    this.btnNoGoBackTxt,
    this.dataManualChangesBottomShtBtnTxt,
  });

  static TranslationModel? getTranslation(Locale? locale) {
    Map<String, TranslationModel> translationList;
    translationList = TranslationTemplate.translationCollection();

    if (locale != null) {
      return translationList[locale.languageCode.toUpperCase()];
    }
    return translationList[nl];
  }
}
