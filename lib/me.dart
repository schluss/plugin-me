library me;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:me/interface/common_plugin_dao.dart';
import 'package:me/ui/personal_info_input_page.dart';

class PluginMe implements CommonPluginDAO {
  final StreamController<int> _streamController = StreamController.broadcast();

  /// Initiates the plug-in for getting user input values.
  @override
  void runPlugin(context, var jsonProvider, var jsonProviderData, var dataModel, Function callBack, {String pluginName = 'ME'}) {
    var locale = Localizations.localeOf(context); // Sets localization.

    Map<String, dynamic>? provider = jsonDecode(jsonProvider);
    Map<String, dynamic>? providerData = jsonDecode(jsonProviderData);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PersonalInfoInputPage(locale, provider, providerData, dataModel, callBack, pluginName: pluginName),
      ),
    );
  }

  /// Extracts data and sets into a map of attributes.
  @override
  Future<Map<String, dynamic>> getExtractedData(String pluginData) async {
    var attributes = <String, String>{};
    var inputData = pluginData.split(',');

    // Maps fields and input values.
    attributes['Street'] = inputData[0];
    attributes['HouseNumber'] = inputData[1];
    attributes['PostalCode'] = inputData[2];
    attributes['City'] = inputData[3];

    _streamController.add(100);

    return Future.value(attributes);
  }

  /// Gets the progress value of the process.
  Stream<int> getProgress() {
    return _streamController.stream; // Returns stream controller to the main app.
  }
}
