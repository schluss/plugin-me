import 'package:me/model/translation_model.dart';

class TranslationTemplate {
  static Map<String, TranslationModel> translationCollection() => {
        'EN': TranslationModel(
          backButtonText: 'Back',
          providerInfoSrcTxt: 'SOURCE',
          dataManualChangesBottomShtTitle: 'Do you want to save your changes?',
          dataManualChangesBottomShtTxt: 'Please indicate whether you would like to save the changes you have made to your personal data.',
          btnNoGoBackTxt: 'No, go back',
          dataManualChangesBottomShtBtnTxt: 'Yes, save changes',
        ),
        'UK': TranslationModel(
          backButtonText: 'Back',
          providerInfoSrcTxt: 'SOURCE',
          dataManualChangesBottomShtTitle: 'Do you want to save your changes?',
          dataManualChangesBottomShtTxt: 'Please indicate whether you would like to save the changes you have made to your personal data.',
          btnNoGoBackTxt: 'No, go back',
          dataManualChangesBottomShtBtnTxt: 'Yes, save changes',
        ),
        'NL': TranslationModel(
          backButtonText: 'Terug',
          providerInfoSrcTxt: 'BRON',
          dataManualChangesBottomShtTitle: 'Wijzigingen opslaan?',
          dataManualChangesBottomShtTxt: 'Geef aan of je de wijzigingen die je gemaakt hebt aan je persoonlijke gegevens wilt opslaan.',
          btnNoGoBackTxt: 'Nee, ga terug',
          dataManualChangesBottomShtBtnTxt: 'Ja, wijzigingen opslaan',
        ),
      };
}
